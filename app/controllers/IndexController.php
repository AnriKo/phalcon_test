<?php

use Phalcon\Mvc\Controller;
//use \app\models\Products;

class IndexController extends Controller
{
    public function indexAction()
    {
    	// Выбираем записи продуктов
		$products = Products::find();

		// передаем в представление
        $this->view->setVar('products', $products);
    }
}