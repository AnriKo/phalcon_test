<?php

use Phalcon\Mvc\Model;

class ProductCategory extends Model
{

    public function initialize()
    {
        $this->setSource('product_category');
    
    }

}