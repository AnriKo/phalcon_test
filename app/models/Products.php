<?php

use Phalcon\Mvc\Model;

class Products extends Model
{
    public function initialize()
    {

        $this->hasManyToMany(
            'id',
            'ProductCategory',
            'id_product', 'id_cat',
            'Categories',
            'id',
            array('alias' => 'categories')
        );
    
    }

}